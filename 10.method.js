// Map => creates a new array with the results of calling a provided function on every element in the calling array.

let arr1 = [1,2,3,4,5]

const map1 = arr1.map(x => x ** 2);

console.log(map1)

//Filter => d creates a new array with all elements that pass the test implemented by the provided function.

var words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

const result = words.filter(word => word != 'spray');

console.log(result);