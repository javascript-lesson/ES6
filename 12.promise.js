// Promise
let success = false;
var promise1 = new Promise(
    function (resolve, reject) {
        if (success) {
            resolve("success"); // fulfilled
        } else {
            reject("fail"); // reject
        }

    }
);

promise1.then((result) => {
    console.log(result)
}).catch(error => {
    console.log(error)
})