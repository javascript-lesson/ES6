// function timeout() {
//     console.log("first")
//     setTimeout(function(){
//         console.log("second")
//     },5000);
//     console.log("third")
// }

// timeout();

//An asynchronous function, which returns an AsyncFunction object
//using an Promise to return its result

//The await operator is used to wait for a Promise. 
//It can only be used inside an async function

function resolveAfter2Seconds() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('resolved');
      }, 2000);
    });
}
  
async function asyncCall() {
    console.log('calling');
    var result = await resolveAfter2Seconds();
    console.log(result);
    console.log("third")
}
  
  asyncCall();