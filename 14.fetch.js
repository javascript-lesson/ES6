//The Fetch API provides a JavaScript interface for accessing and manipulating parts of the HTTP pipeline, such as requests and responses

fetch('http://dummy.restapiexample.com/api/v1/employees')
    .then(response => response.json())
    .then(result => console.log(result))
    .catch(error => console.error('Error:', error));