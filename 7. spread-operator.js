// Array
let arr1 = [1, 2, 3];
arr1 = [...arr1, 4, 5];  

console.log(arr1)

// Object

let obj1 = { name : "Irrahub", email : "irrahub@gmail.com"}
obj1 = { ...obj1, phone : 12345}

console.log(obj1)