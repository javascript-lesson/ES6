// The rest parameters are used to get the argument of an array,
// return a new array.

// function year(){
//     console.log(arguments) 
// }

// year(1990, 1998, 1995, 1993)

function year(...arr){
    console.log(arr) 
}

year(1990, 1998, 1995, 1993)