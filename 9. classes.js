class Rectangle {
    constructor(height, width) {
      this.height = height;
      this.width = width;
    }
   
    calcArea() {
      return this.height * this.width;
    }
}

class SubRectangle extends Rectangle{

    constructor(height, width) {
        super(height, width)
        this.pi = 3.14;
    }

    getwidth(){
        return this.width; 
    }

    getpi(){
        return this.pi; 
    }
}
  
const square = new Rectangle(10, 10);

const square1 = new SubRectangle(10, 10);

console.log(square.calcArea()); // 100

console.log(square1.getwidth()); // 10

console.log(square1.getpi()); // 10